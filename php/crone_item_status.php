<?php
	/**
	* THIS IS A SCRIPT THAT RUNS EVERY MINUTE CHECKS IF ANY ITEM IS FINISHED
	* IF SO THEN IT CHECKS IF IT WAS SOLD AND NOTIFIES THE winner
	* IF ITS NOT SOLD THEN IT RETURNS THE BIDS TO THE BIDDERS WITH +extra bids
	*/
	require_once('model/email.class.php');
	require_once('model/client.class.php');
	//we need to make a query in the database and check the status of the items
	//if an auction has expired we calculate if it met the minimum price ... if yes then we find out the winner
	//we notify the winner and congratulate him
	//otherwise we get all the bidders for that item and return their bids + 20% more
	$connection = mysqli_connect("bestbidc.mysql.db.internal","bestbidc_admin","qTLw4k+H","bestbidc_db") or die('something went wrong!'.mysqli_connect_error());
	$email = new Email();
	//get all the items who are still active but they should be clossed
	$query_deadline = "SELECT *FROM items WHERE deadline<NOW() AND status='active'";
	$result = $connection->query($query_deadline);
	//count the results returned
	$result_rows = mysqli_num_rows($result);
	//if we have items that are finished but they are not processed
	if ($result_rows > 0) {
		$status = '';
		//we have some items who are finished ... we have to check if they met the minimum required price to be sold
		while($row = mysqli_fetch_array($result)) {
			$query_final_price = "SELECT id,current_price FROM auctions WHERE iid=".$row['iid'];
			$query_execute = $connection->query($query_final_price);
        	$data = mysqli_fetch_array($query_execute);
			//auction information
        	$final_price = $data['current_price'];
			$auction_id = $data['id'];
			//if minimum price has been met
			if ($final_price >= $row['min_price']) {
				//the item riched the minimum price
				//$email->itemSoldEmail('Kushtrim','kushtrim.abdiu.16@gmail.com','username','password');
				//change status to sold
				$status = 'sold';
				//inform the person who won
				$query_winner = "SELECT c.name AS client_name, c.surname, c.email, c.address, c.post, b.new_price,i.name AS                                                      item_name,i.brand,i.category FROM clients c, bids b, items i,auctions a WHERE c.cid=b.cid AND                                                     b.aid=".$auction_id." AND a.iid=i.iid AND a.id=b.aid ORDER BY b.new_price DESC LIMIT 1";
				$winner_info = $connection->query($query_winner);
				$winner = mysqli_fetch_array($winner_info);

				$email->notifyWinner($winner);
			}
			else {
                //this is for testing purpose - onAccountCreation
				$email->onAccountCreation('Kushtrim','kushtrim.abdiu.16@gmail.com','username','password');
				//change status to passed
				$status = 'passed';
				$query_bidders = "SELECT c.cid,c.name,c.surname,c.username,c.email,b.cid, COUNT(*) AS frequency,i.name AS item_name FROM  bids                                     b,clients c, items i,auctions a WHERE aid=".$auction_id." AND b.cid=c.cid AND b.aid=a.id AND a.iid=i.iid GROUP                                   BY b.cid";
				$bidders_info = $connection->query($query_bidders);
				//loop through the results and for each person calculate how many bids we should return ..update their bids..and inform them
				//bidders list
				$bidders_array = array();
				while($bidders_row = mysqli_fetch_array($bidders_info)) {
				//we have each user and we see how many bids they have bidded we add +20% of that and then we make a query to update their bids
					$frequency = $bidders_row['frequency'];
					$twenty_percent = floor(($frequency*20)/100);
					$bids_to_return = $frequency + $twenty_percent;
					$client = new Client();
					$client->updateClientBids($bidders_row['username'],$bids_to_return);
                    //notify every bidder the nr of bids we returned them
                    $email->notifyClientForBidsReturned($bidders_row['name'],$bidder_row['email'],$frequency,$twenty_percent,$bidders_row['item_name']);
				}

			}
			//change the status of the item that has finished
			$query_update_status = "UPDATE items SET status='".$status."' WHERE iid=".$row['iid'];
			$result=$connection->query($query_update_status);
		}
	}
    else {
        $email = new Email();
        $to = "kushtrim.abdiu.16@gmail.com";
        $message = "There is no item finished yet!";
        //$email->customEmail($to,$message);
    }

?>
