angular.module('BestBids')
    .controller('buyController', ['$scope','$rootScope','GetPackages','$mdDialog',function($scope,$rootScope,GetPackages,$mdDialog){
		$scope.buying_in_progress = false;
		$scope.selected_package_id = -1;
  		$scope.packages = [];
		GetPackages.getPackages().done(function(data){
			$scope.packages = jQuery.parseJSON(data);
			console.log($scope.packages);
			$scope.$apply();
		});

		// the handler for Stripe payment
		var handler = StripeCheckout.configure({
			key: 'pk_test_xrrdfuCYefSoI8yYclp1Ts9x',
			image: 'images/location/DE/bayern.png',
			token: function(token) {
				// Use the token to create the charge with a server-side script.
				// You can access the token ID with `token.id`
				var jqxhr = $.post( "php/server.php", {command:'stripe-service',stripe:token.id,pid:$scope.selected_package_id},function() {   })
				.done(function(data) {
					var response = jQuery.parseJSON(data);
					if (response.status == 'success') {
						//alert('you purchased successfully');
                        $scope.purchaseWasSuccessfull();
						//update the data
						$rootScope.checkSessionData();
					}
					else if (response.status == 'fail') {
						alert('something went wrong');
					}
					$scope.buying_in_progress = false;
				})
				.fail(function(data) {
				 // alert(JSON.stringify(data));
				})
				.always(function() {

				});
			}
		});

		//package clicked
		$rootScope.buyPackage = function(package_id) {				
			//before buying we have to check if we are logged in IF YES THEN we can proceed
			if ($rootScope.isLoggedIn) {
				for (var i=0; i<$scope.packages.length; i++) {
					if ($scope.packages[i].pid == package_id) {
						$scope.selected_package_id = package_id;
						$scope.buying_in_progress = true;
						$scope.checkOut($scope.packages[i].pid);
					}
				}
			}
			else {
				//alert('you need to loggin');
				$rootScope.loginAndBuyPackage = true;
				$rootScope.currentSelectedPackage = package_id;
				$rootScope.login();
			}
		}

		// Close Checkout on page navigation
		$(window).on('popstate', function() {
			$scope.buying_in_progress = false;
			handler.close();
		});

		$scope.checkOut = function(package_id) {
			handler.open({
			  name: 'Best Bids'
			})
		}


        //message to display after the purchase has been successfully done
        $scope.purchaseWasSuccessfull = function() {
            $mdDialog.show({
    	      controller: purchaseWasSuccessfullDialog,
    	      templateUrl: 'views/purchase-successfull.html',
    	    })

            function purchaseWasSuccessfullDialog($scope) {
    			$scope.okPressed = function() {
    				$mdDialog.hide();
    			}
            }
        }

  }]);
