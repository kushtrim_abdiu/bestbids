angular.module('BestBids')
    .controller('homeController', ['$scope','RecentItems','$location', function($scope,RecentItems,$location){
		//get four latest items
		$scope.latest_items;
		RecentItems.getRecentItems().done(function(data){
			 $scope.latest_items = jQuery.parseJSON(data);
			 console.log($scope.latest_items);
			 if (!$scope.$apply())
			 {
				$scope.$apply();
			 }
		});

        $scope.itemDetailView = function(item_id) {
            $location.path("/item/"+item_id);
        }
        
  }])
.directive('corouselView', function(){
	return {
		restrict:'E',
		templateUrl:'views/corousel-view.html',
		controller: function() {
		}
	};
})
.directive('footerView', function(){
	return {
		restrict:'E',
		templateUrl:'views/footer-view.html',
		controller: function() {
		}
	};
});
