angular.module('BestBids')
    .controller('accountController', ['$scope','$rootScope','$location', function($scope,$rootScope,$location){
		//get four latest items 
		$scope.email_updated = false;
		$scope.password_changed = false;
		$scope.password_changed_error = false;
		$scope.info_updated = false;
		$scope.info_updated_error = false;
		
		$scope.expand_settings = true;
		$scope.expand_notification = true; 
		
		$scope.new_email = '';
		$scope.old_password = '';
		$scope.new_password = '';
		$scope.re_enter_new_password = '';
		
		//update email
		$scope.updateEmail = function() {
			var jqxhr = $.post("php/server.php",{new_email:$scope.new_email,command:'update_email'}, function(){})
			.done(function(data) {
				var response = jQuery.parseJSON(data);
				if (response.status == 'success') {
					$rootScope.userAccountInformation['email'] = $scope.new_email;
					$scope.new_email ='';
					$scope.email_updated = true;
				}
				$scope.$apply();
			});
		}
		
		//update password
		$scope.updatePassword = function() {
			var jqxhr = $.post("php/server.php",{old_pass:$scope.old_password,new_pass:$scope.new_password,command:'update_password'}, function(){})
			.done(function(data) {
				var response = jQuery.parseJSON(data);
				if (response.status == 'success') {
					//the password was changed correctly 
					$scope.password_changed = true;
				}
				else if (response.status =='fail') {
					$scope.password_changed_error = true;
				}
				$scope.old_password = '';
				$scope.new_password = '';
				$scope.re_enter_new_password = '';
				console.log(data);
				$scope.$apply();
			});
		}
		
		//update personal info 
		$scope.updateInfo = function () {
			var jqxhr = $.post("php/server.php",{user:$rootScope.userAccountInformation,command:'update_user_account_info'}, function(){})
			.done(function(data) {
				var response = jQuery.parseJSON(data);
				if (response.status == 'success') {
					$scope.info_updated = true;
				}
				else if (response.status == 'fail') {
					$scope.info_updated_error = true;
				}
			});
		}
		
		$scope.toggleNotification = function() {
			$scope.expand_notification = !$scope.expand_notification;
			alert($scope.expand_notification);
		}
		
		$scope.toggleSettings = function() {
			$scope.expand_notification = !$scope.expand_settings;
		}
		
		//go to buy bids
		$scope.goToBuyBids = function() {
            $location.path("/buy");
        }
  }])