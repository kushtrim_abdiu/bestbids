angular.module('BestBids')
.controller('liveController', ['$scope','$rootScope','LiveItems','$location', function($scope,$rootScope,LiveItems,$location){
	//flag to show if there is items or not
	$scope.has_items = false;
	$scope.total_auctions = 0;
	//all live items
	$scope.live_items;
	//holds the currently selected category
	$scope.selectedCategory = "KATEGORIEN";
	$scope.categories = ['KATEGORIEN','Accessories','Clutches','Handtaschen','Lederwaren','Gutscheine','Uhren','Elektronik'];
	
	//get live items
	LiveItems.getRecentItems().done(function(data){
        console.log(data);
		$scope.live_items = jQuery.parseJSON(data);
		console.log('these are the items');
		console.log($scope.live_items);
		$scope.total_auctions = $scope.live_items.length;
		if ($scope.total_auctions >0) {
			$scope.has_items = true;
		}
		$scope.$apply();
	});
	
	//when filtering by category 
	$scope.filterByCategory = function(category) {
		//filter the current items according to the category 
		var counter = 0;
		for (var i=0; i<$scope.live_items.length; i++) {
			if ($scope.live_items[i].category == category) {
				counter++;
			}
		}
		if (counter == 0) {
			$scope.has_items = false;
		}
		else {
			$scope.has_items = true;
		}
	}
	
	$scope.itemDetailView = function(id) {
		$location.path("/item/"+id);
	}
}])