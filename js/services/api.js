angular.module('BestBids')
.factory('RecentItems', ['$http', function($http){
	return{
		getRecentItems : function() {
			return  $.post( "php/server.php", {command:'latest-auctions'},function() {})
    }
 } 
}])
.factory('LiveItems', ['$http', function($http){
	return{
    getRecentItems : function() {
        return  $.post( "php/server.php", {command:'auctions'},function() {})
    }
 } 
}])
.factory('ClosedItems', ['$http', function($http){
	return{
    getRecentItems : function() {
        return  $.post( "php/server.php", {command:'closed-auctions'},function() {})
    }
 }	
}])
.factory('GetPackages', ['$http', function($http){
	return{
    getPackages : function() {
        return  $.post( "php/server.php", {command:'get-packages'},function() {})
    }
 }	
}])
.factory('UserDetails', ['$http', function($http){
	return{
    getPackages : function() {
        return  $.post( "php/server.php", {command:'get-packages'},function() {})
    }
 }	
}])
.factory('ItemDetails', ['$http', function($http){
	return{
    getPackages : function(id) {
        return  $.post( "php/server.php", {iid:id,command:'item-information'},function() {})
    }
 }	
}]);