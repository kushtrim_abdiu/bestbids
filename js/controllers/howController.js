angular.module('BestBids')
  .controller('howController', ['$scope','ClosedItems','$location',function($scope,ClosedItems,$location){
  		$scope.items={};
		ClosedItems.getRecentItems().done(function(data){
			$scope.items = jQuery.parseJSON(data);
			console.log($scope.items);
			$scope.$apply();
		});

        $scope.goToLiveAuction = function() {
            $location.path("/live");
        }
        $scope.goToRegistration = function() {
            $location.path("/register");
        }
        $scope.goToBuyBids = function() {
            $location.path("/buy");
        }
  }]);
