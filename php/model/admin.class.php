<?php

/**
*  This is the ADMIN class that handles the admin part of the application
*  Considering that others can access it we should secure the
*/
class Admin
{
	/**
	* const the database user
	*/
	const $DB_USER = 'root';

	/**
	* const the database password
	*/
	const $DB_PASSWORD = 'qTLw4k+H';

	/**
	* const the host
	*/
	const $DB_HOST = 'localhost';

	/**
	* const the database name
	*/
	const $DB_NAME = 'bestbidcdb';

	/**
	* the connection
	*/
	private $CONNECTION;

	//constructor
	public function __construct(){
		$this->CONNECTION = mysqli_connect(self::DB_HOST, self::DB_USER, self::DB_PASSWORD, self::DB_NAME)
			or die('something went wrong!'.mysqli_connect_error());
	}

	/**
	* getNumberOfClients
	* @return Integer the number of clients
	*/
	public function getNumberOfClients()
	{
		$query = sprintf("SELECT COUNT(cid) AS total FROM clients");
		$result = $this->CONNECTION->query($query);
		$data = mysqli_fetch_assoc($result);

		return $data['total'];
	}

	/**
	* getNumberOfItems
	* @return Integer the number of items
	*/
	public function getNumberOfItems()
	{
		$query = sprintf("SELECT COUNT(iid) AS total FROM items");
		$result = $this->CONNECTION->query($query);
		$data = mysqli_fetch_assoc($result);

		return $data['total'];
	}

	/**
	* getNumberOfActiveItems - retruns the number of items that arent sold yet
	* @return Integer the number of active items
	*/
	public function getNumberOfActiveItems()
	{
		$query = sprintf("SELECT COUNT(iid) AS total FROM items WHERE status = 'active'");
		$result = $this->CONNECTION->query($query);
		$data = mysqli_fetch_assoc($result);

		return $data['total'];
	}

	/**
	* getNumberOfSoldItems
	* @return Integer the number of sold items
	*/
	public function getNumberOfSoldItems()
	{
		$query = sprintf("SELECT COUNT(iid) AS total FROM items WHERE status='sold'");
		$result = $this->CONNECTION->query($query);
		$data = mysqli_fetch_assoc($result);

		return $data['total'];
	}

	/**
	* getNumberOfPassedItems - is the number of items that weren't sold
	* and the bidding is closed
	* @return Integer the number of items
	*/
	public function getNumberOfPassedItems()
	{
		$query=sprintf("SELECT COUNT(iid) AS total FROM items WHERE status='passed'");
		$result = $this->CONNECTION->query($query);
		$data=mysqli_fetch_assoc($result);

		return $data['total'];
	}

	//TO DO : Finish this method
	public function getTotalPackageSold()
	{
	$query=sprintf("SELECT SUM()");
	}

	/**
	* uploadNewItem
	* @param String $name - the item name
	* @param String $brand - is the brand of the item
	* @param String $category - the item category i.e. electronics, bags, watch etc..
	* @param Double $min_price - is the minimum price that the item needs to be sold
	* @param Double $retail_price - is the price at which the item can be bought in retail
	* @param DateTime $deadline - is the deadline when the bidding finishes
	* @param String $description - is the item description details
	* @param String $status - is the item status e.g: in auction or etc..
	* @param String $img_url - this is the url where the image of this item was uploaded
	* @return Integer the id of the created item
	*/
	public function uploadNewItem($name, $brand, $category, $min_price, $retail_price, $deadline, $description, $status, $img_url)
	{
		$sql = "INSERT INTO items(name, brand, category, min_price, retail_price, deadline, description,status,img) ".
			"VALUES('".$name."','".$brand."','".$category."','".$min_price."','".$retail_price."','".$deadline."','"
			.$description."','".$status."','".$img_url."')";

		if ($this->CONNECTION->query($sql) === TRUE) {
			$id = mysqli_insert_id($this->CONNECTION);
			return $id;
		}
		else {
			return "Error: " . $sql . "<br>" . $db->error;
		}
	}

	/**
	* createNewAuction - sets an item to auction
	* @param Integer $iid - the item id which will be put in auction
	* @return String - success|error
	*/
	public function createNewAuction($iid){
		$date = date('Y-m-d H:i:s');
		$sql = "INSERT INTO auctions(iid,`date`,current_price) VALUES ('".$iid."','".$date."',0)";
		if($this->CONNECTION->query($sql) === TRUE){
			echo 'successful';
		}
		else{
			echo "Error: " . $sql . "<br>" . $this->CONNECTION->error;
		}
	}

	/**
	* getBidPackages - returns the bid packages
	* @return Array $packages
	*/
	public function getBidPackages()
	{
		$packages = array();
		$sql = "SELECT *FROM packages";
		$result = $this->CONNECTION->query($sql);
		while($row = mysqli_fetch_assoc($result)) {
			array_push($packages,$row);
		}

		return $packages;
	}

	/**
	* getPackageInfo - gets the details of the package selected
	* @param Integer $package_id - the package id
	* @return String - success|error
	*/
	public function getPackageInfo($package_id) {
		$id = mysqli_real_escape_string($this->CONNECTION, $package_id);
		$query = sprintf("SELECT pid,bid_number,free_bids,price FROM packages WHERE pid='$id'");
		$result = $this->CONNECTION->query($query);
		$data = mysqli_fetch_assoc($result);

		return $data;
	}

}
?>
