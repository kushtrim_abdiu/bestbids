<?php
	require_once("model/client.class.php");
	class Item_JSON
	{
		public $json;
	
		//constructor
		public function __construct($iid)
		{
			$this->json = array();
            $this->check_if_json_is_set($iid);
        }
        
        //checks if the json file is empty or not .. if its empty we query the database to get all active items and create json out of them
        private function check_if_json_is_set($iid)
        {
	        $json_array = json_decode(file_get_contents("items.json"));
	        $json_decoded = json_decode(json_encode($json_array),true);
	        //check if there is any element and also if the current item is in the file
	        if (count($json_array) == 0 || !isset($json_decoded[$iid])) {
		        $items_array = array();
		        //no elements yet.. or the JSON is not created so we have to read from db
		        $client = new Client();
		        $hash_items = $client->getAllActiveItems();
		        if (count($hash_items)>0) {
			        foreach	($hash_items as $item) {
				        $item_key = $item['iid'];
				        $items_array[$item_key] = 0;
			        }			        
			        //write to the file 
			        file_put_contents('items.json', json_encode($items_array, JSON_FORCE_OBJECT));
		        } 
		       
	        }
	        
	        $this->json = json_decode(file_get_contents("items.json"));
	        

        }
        
        //this when we query the server if we have new bid
        public function get_timestamp_for_item($iid)
        {
	        $id = intval($iid);
	        $array = json_decode(json_encode($this->json), true);
			
	        return $array[$id];
        }
        
        //this will be called on bid 
        public function update_timestamp_for_item($iid)
        {
	        $id = intval($iid);
	        $array = json_decode(json_encode($this->json), true);
	        $array[$id] = time();
	        file_put_contents('items.json', json_encode($array, JSON_FORCE_OBJECT));
        }
	}
?>