<?php
/**
* This is the client class
* the database connection information should be into a different file
* and reused in the other classes
*/
class Client
{
    /**
	* const the database user
	*/
	const $DB_USER = 'root';

	/**
	* const the database password
	*/
	const $DB_PASSWORD = 'qTLw4k+H';

	/**
	* const the host
	*/
	const $DB_HOST = 'localhost';

	/**
	* const the database name
	*/
	const $DB_NAME = 'bestbidcdb';

	/**
	* the connection
	*/
	private $CONNECTION;

	//constructor
	public function __construct(){
		$this->CONNECTION = mysqli_connect(self::DB_HOST, self::DB_USER, self::DB_PASSWORD, self::DB_NAME)
			or die('something went wrong!'.mysqli_connect_error());
	}

    /**
	* authenticateClient
    * @param String $username
    * @param String $password
	* @return Array $arrayData
	*/
    public function authenticateClient($username, $password)
    {
        $arrayData = array();
        //security check
        $user_name = mysqli_real_escape_string($this->CONNECTION,$username);
        $user_password = mysqli_real_escape_string($this->CONNECTION,$password);

        $query = sprintf("SELECT c.cid, c.name as first_name, c.surname, c.email, c.username, c.address, ".
            "c.post, c.bids, l.name as location_name, l.country FROM clients c, locations l where c.lid = l.lid and ".
            "c.username='".$user_name."' and c.password='".$user_password."'");
        $result = $this->CONNECTION->query($query);
        $result_rows = mysqli_num_rows($result);
        $row = mysqli_fetch_assoc($result);

        if ($result_rows == 1) {
            $arrayData = $row;
            $arrayData['status'] = 'success';
        }
        else {
            $arrayData['status']='fail';
        }

        return $arrayData;
    }

    /**
    * insertClient - creates a new user
    * @param String $name
    * @param String surname
    * @param String $email
    * @param String $username
    * @param String $password
    * @param String $address
    * @param String $post - is the post number
    * @param Integer $bids - is the number of bids the user gets
    * @param String $canton - is the canton/region
    * @param String $code - is the promotion code that the user got from any of his friends
    * @return Integer the number of active items
    */
    public function insertClient($name, $surname, $email, $username, $password, $address, $post, $bids, $canton, $code)
    {
        $status = 'fail';
        //escaping all the strings to avoid SQL injections
        $user_name      = mysqli_real_escape_string($this->CONNECTION, $name);
        $user_name      = mysqli_real_escape_string($this->CONNECTION, $name);
        $user_surname   = mysqli_real_escape_string($this->CONNECTION, $surname);
        $user_email     = mysqli_real_escape_string($this->CONNECTION, $email);
        $user_username  = mysqli_real_escape_string($this->CONNECTION, $username);
        $user_password  = mysqli_real_escape_string($this->CONNECTION, $password);
        $user_address   = mysqli_real_escape_string($this->CONNECTION, $address);
        $user_post      = mysqli_real_escape_string($this->CONNECTION, $post);
        $user_canton    = mysqli_real_escape_string($this->CONNECTION, $canton);

        $sql = "INSERT INTO clients(name, surname, email, username, password, address, post,bids,lid,promotional_code) ".
            "VALUES('".$user_name."','".$user_surname."','".$user_email."','".$user_username."','".$user_password."','"
            .$user_address."','".$user_post."','".$bids."','".$user_canton."','".$code."')";

        if ($this->CONNECTION->query($sql) === TRUE) {
            $status = 'success';
        }

        return $status;
    }

    /**
    * validate_promo_code - this function validates if the promotional code is correctß
    * @param String $code - the prommo code the user entered
    * @return Boolean
    */
    public function validate_promo_code($code) {
        $promotional_code = mysqli_real_escape_string($this->CONNECTION, $code);

        $second_query=sprintf("SELECT cid FROM clients WHERE promotional_code = '".$code."'");
        $result = $this->CONNECTION->query($second_query);
        $result_rows = mysqli_num_rows($result);

        if ($result_rows == 1) {
            $query = sprintf("UPDATE clients SET `bids` = bids+3 WHERE promotional_code = '".$code."'");
            $result = $this->CONNECTION->query($query);
            return true;
        }
        else {
            return false;
        }
    }

    /**
    * checkIfUsernameExists - checks if the username is correct
    * @param String $username
    * @return String - the status new|exists
    */
    public function checkIfUsernameExists($username){
        $answerArray = array();
        $user_name = stripcslashes($username);
        $query = sprintf("SELECT cid FROM clients WHERE username = '".$user_name."'");
        $result = $this->CONNECTION->query($query);
        $num_rows = mysqli_num_rows($result);

        if ($num_rows == 0) {
            //doesnt exist any user with that username
            return 'new';
        }
        else{
            return 'exists';
        }
    }

    /**
    * getClientBids - returns the amount of bids the client has
    * @param String $client_username - the username
    * @return Integer $number_of_bids
    */
    public function getClientBids($client_username)
    {
        $number_of_bids = 0;
        $query=sprintf("SELECT bids FROM clients WHERE username = '".$client_username."'");
        $result = $this->CONNECTION->query($query);
        $result_rows = mysqli_num_rows($result);

        if ($result_rows == 1) {
            //if we have such client
            while($row = mysqli_fetch_array($result)) {
                $number_of_bids = $row["bids"];
            }
        }

        return $number_of_bids;
    }

    /**
    * updateClientBidsByOne
    * @param String $client_username - the username
    * @return void
    */
    public function updateClientBidsByOne($client_username)
    {
        $query = sprintf("UPDATE clients SET `bids`= bids + 1 WHERE username = '".$client_username."'");
        $result = $this->CONNECTION->query($query);
        if($this->CONNECTION->query($query) === TRUE) {
            echo 'successful!';
        }
        else{
            echo "Error: " . $sql . "<br>" . $db->error;
        }
    }

    /**
    * updateClientBids
    * @param String $client_username - the username
    * @param Integer $new_bid_number
    * @return String status
    */
    public function updateClientBids($client_username, $new_bid_number)
    {
        echo 'these are the new bids'.$new_bid_number.'<br>';
        $query=sprintf("UPDATE clients SET `bids`=bids+'".$new_bid_number."' WHERE username='".$client_username."'");
        if ($this->CONNECTION->query($query) === TRUE) {
            echo 'successful!';
        }
        else{
            echo "Error: " . $this->CONNECTION . "<br>" . $$THIS->CONNECTION->error;
        }
    }

    /**
    * getAllClients
    */
    public function getAllClients()
    {
        $query = sprintf('SELECT * FROM `clients` WHERE 1');
        $result = $this->CONNECTION->query($query);
        while($row = mysqli_fetch_array($result)){
          echo $row["name"] . "<br>";
        }
    }

    /**
    * getNumberOfClients
    * @return String status
    */
    public function getNumberOfClients()
    {

        $query=sprintf("SELECT COUNT(cid) AS total FROM clients");
        $result = $this->CONNECTION->query($query);
        $data=mysqli_fetch_assoc($result);

        return $data['total'];
     }

     /**
     * getNumberOfClients
     * @return Array $hashArray
     */
     public function getAllActiveItems()
     {
        $hashArray = array();
        $sql = sprintf("SELECT i.iid,name,brand,category,min_price,retail_price,deadline,description,"
            ."img,current_price FROM auctions a, items i WHERE a.iid=i.iid AND i.status='active'");
        $result = $this->CONNECTION->query($sql);
        while($row = mysqli_fetch_array($result)) {
            $row["img"] = "images/items/".$row["img"];
            array_push($hashArray, $row);
        }

         return $hashArray;
     }

    /**
    * getActiveItemsOfCategory
    * @param String $category
    * @return Array $hashArray
    */
    public function getActiveItemsOfCategory($category)
    {
        $hashArray = array();
        $sql = "";
        if ($category == "Kategorien" || $category == "Marken") {
            $sql = sprintf("SELECT * FROM items WHERE status = 'active'");
        }
        else {
            $sql = sprintf("SELECT * FROM items WHERE status = 'active' AND category = '".$category."'");
        }
        $result = $this->CONNECTION->query($sql);

        while($row = mysqli_fetch_array($result)) {
            $row["img"] = "images/items/".$row["img"];
            array_push($hashArray, $row);
        }

         return $hashArray;
    }

    /**
    * getAllClosedAuctions
    * @return Array $hashArray
    */
    public function getAllClosedAuctions(){
        $hashArray = array();

        $sql=sprintf("SELECT * FROM items WHERE status='passed' OR status='sold' ORDER BY deadline DESC LIMIT 5");
        $result = $this->CONNECTION->query($sql);
        $num_rows = mysqli_num_rows($result);
        if ($num_rows != 0) {
            while($row = mysqli_fetch_array($result)) {
                $row['stat']='notempty';
                $row["img"] = "images/items/".$row["img"];

                array_push($hashArray, $row);
                if ($row['status'] == 'sold') {
                    $sql_get_username = sprintf("SELECT c.username, MAX(b.new_price) as last_price FROM clients c, bids b, auctions a WHERE a.id=b.aid AND a.iid=".$row['iid']);
                    $result_name = $this->CONNECTION->query($sql_get_username);
                    $response = mysqli_fetch_array($result_name);
                    $row['won_by'] = 'won by: '.$response['username'];
                }
                else {
                    $row['won_by'] = $row['status'];
                }

                array_push($hashArray, $row);
              }

             return $hashArray;
        }
        else{
             $row['stat'] = 'empty';
             array_push($hashArray, $row);

             return $row;
        }
    }
     /**
     * THIS FUNCTION RETURNS ALL THE INFORMATION FOR THE SPECIFIED AUCTIONED ITEM
     * @return $arrayData of type associative array
     */
     public function getItem($ID)
     {
        $arrayData =array();
        $sql=sprintf("SELECT i.iid, name, brand, category, min_price, retail_price, deadline, description, img, current_price ".
            "FROM auctions a, items i WHERE a.iid=i.iid AND i.iid='".$ID."'");
        $result = $this->CONNECTION->query($sql);

        while($row = mysqli_fetch_array($result)) {
            $row["img"] = "images/items/".$row["img"];
            $arrayData = $row;
        }

        return $arrayData;
     }

    /**
    * bid - bids on the specified item
    * @return void
    */
    public function bid($iid, $cid)
    {
        //default bid value
        $bid_value = 0.01;
        //first we need to know the current value of the item
        $sql_get_current_value = sprintf('SELECT id,current_price FROM auctions WHERE iid="'.$iid.'"');
        $results = $this->CONNECTION->query($sql_get_current_value);
        $data = mysqli_fetch_assoc($results);
        $current_price = $data['current_price'];
        $auction_id = $data['id'];
        //now we know the current price and we have to know the value of this current bid
        //if the time left is more than 15 seconds the value is 0.01
        //if it has les then 15 seconds left then the value is 0.1 and the time needs to be updated
        $sql_get_item_deadline = sprintf("SELECT `deadline` FROM items WHERE iid='".$iid."'");
        $res = $this->CONNECTION->query($sql_get_item_deadline);
        $item_deadline = mysqli_fetch_assoc($res);
        $item_deadline = $item_deadline['deadline'];
        //take the current time and the deadline and check how many seconds are left
        $current_time = date('Y-m-d H:i:s');
        $time_left = strtotime($item_deadline)-strtotime($current_time);
        //if the time left is less than 15 seconds
        if($time_left < 15){
            //bid value is 0.1
            $bid_value = 0.1;
            $new_time_unix = strtotime($item_deadline)+15;
            $new_time = date("Y-m-d H:i:s",$new_time_unix);
            //we have to insert the new time into the database
            $sql_update_item_deadline = sprintf("UPDATE items SET deadline='".$new_time."' WHERE iid='".$iid."'");
            $result=$this->CONNECTION->query($sql_update_item_deadline);

        }
        //decrease the user's bids by one
        //increase the new price in the auctions
        //insert the user bid in the bid table
        $sql_decrease_user_bids = sprintf("UPDATE clients SET bids=(bids-1) WHERE cid='".$cid."'");
        $decreased_bids = $this->CONNECTION->query($sql_decrease_user_bids);
        //increase the new price in the auctions
        $new_price = $current_price + $bid_value;
        $sql_update_current_price = sprintf("UPDATE auctions SET current_price='".$new_price."' WHERE iid='".$iid."'");
        $increase_current_price = $this->CONNECTION->query($sql_update_current_price);
        //now insert the bid in the bid table
        $sql_insert_bid = sprintf("INSERT INTO bids(aid,cid,`date`,new_price) VALUES('".$auction_id."','".$cid."','".$current_time."','".$new_price."')");
        $insert_bid = $this->CONNECTION->query($sql_insert_bid);
    }

    /**
    * get_item_deadline - returns the deadline of the specified item
    * @param Integer $iid
    * @return Date $item_deadline
    */
    private function get_item_deadline($iid)
    {
        $item_id = mysqli_real_escape_string($this->CONNECTION, $iid);
        $sql_get_item_deadline = "SELECT deadline FROM items WHERE iid=".$item_id;
        $res = $this->CONNECTION->query($sql_get_item_deadline);
        $item_deadline = mysqli_fetch_assoc($res);
        $item_deadline = $item_deadline['deadline'];

        return $item_deadline;
    }

    /**
    * update_item_deadline - adds +16 more seconds to the deadline
    * @param Integer $iid
    * @return void
    */
    private function update_item_deadline($iid)
    {
        $current_time = date('Y-m-d H:i:s');
        $new_time_unix = strtotime($current_time) + 16;
        $new_time = date("Y-m-d H:i:s",$new_time_unix);
        //we have to insert the new time into the database
        $sql_update_item_deadline = sprintf("UPDATE items SET deadline='".$new_time."' WHERE iid='".$iid."'");
        $result = $this->CONNECTION->query($sql_update_item_deadline);
    }

    /**
    * bid_on_item - bids on the specified item
    * @param Integer $iid
    * @param Integer $cid
    * @param Double $bid_value
    * @return void
    */
    private function bid_on_item($iid, $cid, $bid_value)
    {
        $sql_get_current_value = sprintf('SELECT id,current_price FROM auctions WHERE iid = "'.$iid.'"');
        $results = $this->CONNECTION->query($sql_get_current_value);
        $data = mysqli_fetch_assoc($results);
        $current_price = $data['current_price'];
        $auction_id = $data['id'];
        $new_price = $current_price+$bid_value;

        $current_time = date('Y-m-d H:i:s');
        $sql_insert_bid = sprintf("INSERT INTO bids(aid,cid,`date`,new_price) VALUES('".$auction_id."','".$cid."','".$current_time."','".$new_price."')");
        $insert_bid = $this->CONNECTION->query($sql_insert_bid);

        $sql_decrease_user_bids = sprintf("UPDATE clients SET bids = (bids-1) WHERE cid = '".$cid."'");
        $decreased_bids = $this->CONNECTION->query($sql_decrease_user_bids);
        //update the session
        //$_SESSION['logged_user']['bids'] = $_SESSION['logged_user']['bids'] - 1;
        $_SESSION['logged_user']['bids'] = $this->getUserBids($cid);
        //update the price in the auctions table
        $sql_update_current_price = sprintf("UPDATE auctions SET current_price = (current_price+'".$bid_value."') WHERE iid = '".$iid."'");
        $increase_current_price = $this->CONNECTION->query($sql_update_current_price);
    }

    /**
    * placeBid - registers the bid
    * @param Integer $iid
    * @param Integer $cid
    * @return Array $arrayResponse
    */
    public function placeBid($iid, $cid)
    {
        $arrayResponse = array();
        //double check if we have enaugh bids
        if ($_SESSION['logged_user']['bids'] > 0) {
            //we have enaugh bids we should make sure that the item is still alive i.e. the auction hasnt finished
            $item_deadline = $this->get_item_deadline($iid);
            $current_time = date('Y-m-d H:i:s');
            $time_left = strtotime($item_deadline)-strtotime($current_time);
            if ($time_left > 0 && $time_left < 16) {
                //the bid value is higher
                $this->bid_on_item($iid, $cid, 0.1);
                //we have to reset the time to 15 seconds
                $this->update_item_deadline($iid);
                $arrayResponse['status'] = 'high_bid';
            }
            else if ($time_left > 16) {
                $this->bid_on_item($iid,$cid,0.01);
                $arrayResponse['status'] = 'normal_bid';
            }
            else if ($time_left < 0) {
                //if its zero second left the auction is closed
                $arrayResponse['status'] = 'auction_closed';
            }
        }
        else {
            $arrayResponse['status'] = 'no_bids';
        }

        return $arrayResponse;
    }

    /**
    * getLastBidersOfItem - registers the bid
    * @param Integer $iid
    * @return Array $bidersArray
    */
    public function getLastBidersOfItem($iid)
    {
        $bidersArray = array();
        $sql_get_last_biders_info = sprintf("SELECT l.name, l.country, c.username, b.new_price ".
            "FROM locations l, clients c, bids b, auctions a WHERE a.iid='".$iid.
            "' AND a.id=b.aid AND c.lid=l.lid AND c.cid=b.cid ORDER BY b.new_price DESC LIMIT 5");
        $result = $this->CONNECTION->query($sql_get_last_biders_info);
        while ($row = mysqli_fetch_array($result)) {
            $tmpArray =array();
            $tmpArray['location']=$row['name'];
            $tmpArray['country'] = $row['country'];
            $tmpArray['username']=$row['username'];
            $tmpArray['bid']=$row['new_price'];

            array_push($bidersArray,$tmpArray);
        }

        return $bidersArray;
    }

    /**
    * getCantons - retrives all the cantons / regions of the specified country
    * @param String $country
    * @return Array $cantonsArray
    */
    public function getCantons($country)
    {
        $cantonsArray = array();
        $current_country = stripslashes($country);
        $sql_get_cantons = sprintf("SELECT DISTINCT(name) FROM locations WHERE country = '".$current_country."'");
        $result = $this->CONNECTION->query($sql_get_cantons);
        while($row = mysqli_fetch_array($result)){
            $canton_name = strval($row['name']);
            $success = settype($canton_name, 'string');
            array_push($cantonsArray, $canton_name);
        }
        return $cantonsArray;
    }

    /**
    * getAllBrands - gets all the brands
    * @return Array $arrayBrands
    */
    public function getAllBrands()
    {
        $arrayBrands = array();
        $sql_brands = sprintf("SELECT DISTINCT(brand) FROM items");
        $results=$this->CONNECTION->query($sql_brands);
        while($row=mysqli_fetch_array($results)){
            array_push($arrayBrands,$row['brand']);
        }
        return $arrayBrands;
    }

    /**
    * getItemsOfBrand - gets all the items of brand
    * @param String $brand
    * @return Array $hashArray
    */
    public function getItemsOfBrand($brand)
    {
        $hashArray = array();
        $sql = '';
        if ($brand == 'Marken') {
            $sql = sprintf("SELECT * FROM items WHERE status='active'");
        }
        else {
            $sql = sprintf("SELECT * FROM items WHERE status = 'active' AND brand = '".$brand."'");
        }
        $result = $this->CONNECTION->query($sql);

        while($row = mysqli_fetch_array($result)) {
            $row["img"] = "images/items/".$row["img"];
            array_push($hashArray, $row);
        }

        return $hashArray;
    }

    /**
    * checkItemDeadline
    * @param Integer $iid
    * @return DateTime $deadline
    */
    public function checkItemDeadline($iid)
    {
        $sql = sprintf("SELECT deadline from items WHERE iid='".$iid."'");
        $result = $this->CONNECTION->query($sql);
        while($row = mysqli_fetch_array($result)){
          return $row['deadline'];
        }
    }

    /**
    * getUserBids
    * @param Integer $cid
    * @return Integer bids
    */
    public function getUserBids($cid)
    {
        $sql = sprintf("SELECT bids from clients WHERE cid = '".$cid."'");
        $result = $this->CONNECTION->query($sql);
        $data = mysqli_fetch_array($result);
        return $data['bids'];
    }

    /**
    * getCantonID - returns the id of the cantont
    * @param String $name
    * @return Integer $cantonID
    */
    public function getCantonID($name)
    {
        $sql_canton_id = sprintf("SELECT lid FROM locations WHERE name = '".$name."'");
        $result = $this->CONNECTION->query($sql_canton_id);
        $data = mysqli_fetch_assoc($result);
        $cantonID = $data['lid'];

        return $cantonID;
    }

    /**
    * validateEmail
    * @param String $email
    * @return String exists|new
    */
    public function validateEmail($email)
    {
        $clean_email = stripslashes($email);
        $sql_email = sprintf("SELECT COUNT(email) AS count FROM clients WHERE email = '".$clean_email."'");
        $result = $this->CONNECTION->query($sql_email);
        $data = mysqli_fetch_assoc($result);

        if ($data['count'] > 0) {
            return 'exists';
        }
        else {
            return 'new';
        }
    }

    /**
    * getLatestItems
    * @param String $email
    * @return Array $arrayData
    */
    public function getLatestItems(){
        $arrayData = array();
        $sql=sprintf("SELECT name, brand, category, min_price, retail_price, deadline, description, img ".
            "FROM items ORDER BY iid DESC LIMIT 4");
        $result = $this->CONNECTION->query($sql);
        while($row = mysqli_fetch_array($result)) {
            $row["img"] = "images/items/".$row["img"];
            $arrayData = $row;
        }

        return $arrayData;
    }
    //this function checks the items that their time has passed
    //and updates their statuses accordingly i.e. - SOLD or PASSED
    public function refreshItemStatuses($activeItems)
    {
        $current_time = date('Y-m-d H:i:s');
        foreach ($activeItems as $item) {
            $time_left = strtotime($item['duedate']) - strtotime($current_time);
            if ($time_left < 0) {
                //this is item that the deadline has passed
                //we have to take its ID and minimum price and see if this item has passed the minimum selling price
                $minimum_selling_price = $item['minprice'];
                $item_id = $item['iid'];
                $sql_get_item_current_price = sprintf("SELECT current_price FROM auctions WHERE iid = '".$item_id."'");
                $result = $this->CONNECTION->query($sql_get_item_current_price);
                $array_item_latest_price = mysqli_fetch_array($result);
                $item_latest_price = $array_item_latest_price['current_price'];
                if ($minimum_selling_price > $item_latest_price) {
                    //this means that the item didnt get sold
                    //we should change the status to passed
                    $sql_update_item_status = sprintf("UPDATE items SET status='passed' WHERE iid='".$item_id."'");
                    $result = $this->CONNECTION->query($sql_update_item_status);
                    //if it was passed we need to take all the biders of this item and give them back the bids
                }
                else{
                    //the item was sold we should change the status to sold
                    $sql_update_item_status = sprintf("UPDATE items SET status = 'sold' WHERE iid ='".$item_id."'");
                    $result = $this->CONNECTION->query($sql_update_item_status);
                }
            }
        }
    }

    /**
    * buyPackage
    * @param String $package
    * @return Array $response
    */
    public function buyPackage($package)
    {
        $response = array();
        $current_time = date('Y-m-d H:i:s');
        $userID = $_SESSION['logged_user']['cid'];
        $total_bids = ($package['bid_number'] + $package['free_bids']);

        //now we have to insert them into buy_package
        $sql_insert_into_buy_package = sprintf("INSERT INTO buy_package(cid, pid, date) VALUES('".$userID."','".$package['pid']."','".$current_time."')");
        $exec = $this->CONNECTION->query($sql_insert_into_buy_package);
        //insert the new bids to the user
        $query = sprintf("UPDATE clients SET bids=bids+'".$total_bids."' WHERE cid='".$userID."'");
        if($exec = $this->CONNECTION->query($query)) {
            $response['status'] = 'success';
            $_SESSION['logged_user']['bids'] = $this->getUserBids($userID);
        }
        else {
            $response['status'] = 'fail';
        }

        return $response;
    }

    /**
    * getLatestActiveItems
    * @return Array $hashArray
    */
    public function getLatestActiveItems()
    {
        $hashArray = array();
        $arrayData = array();
        $sql = sprintf("SELECT i.iid,name,brand,category,min_price,retail_price,deadline,description,".
            "img,current_price FROM auctions a, items i WHERE a.iid=i.iid AND i.status='active' ORDER BY a.date DESC LIMIT 4");
        $result = $this->CONNECTION->query($sql);
        while($row = mysqli_fetch_array($result)) {
            $row["img"] = "images/items/".$row["img"];
            array_push($hashArray, $row);
        }

         return $hashArray;
    }

    /**
    * get_client_promo_code
    * @return Integer promo_code
    */
    public function get_client_promo_code($username_input)
    {
        $username = mysqli_real_escape_string($this->CONNECTION, $username_input);
        $query = sprintf("SELECT promotional_code FROM clients WHERE username = '$username'");
        $result = $this->CONNECTION->query($query);
        $result_rows = mysqli_num_rows($result);
        $row = mysqli_fetch_assoc($result);
        if ($result_rows == 1) {
            return $row['promotional_code'];
        }
        else{
            return 0;
        }
    }

    /**
    * updateClientEmail
    * @param String $new_email
    * @return Array $response
    */
    public function updateClientEmail($new_email) {
        //escape the string
        $response = array();
        $new_email_address = mysqli_real_escape_string($this->CONNECTION, $new_email);
        $old_email_address = $_SESSION['logged_user']['email'];
        $client_id = $_SESSION['logged_user']['cid'];
        $query = sprintf("UPDATE clients SET email='$new_email_address' WHERE cid = '$client_id'");

        //execute the query
        if($exec = $this->CONNECTION->query($query)) {
            $response['status'] = 'success';
            $_SESSION['logged_user']['email'] = $new_email_address;
        }
        else {
            $response['status'] = 'fail';
        }

        return $response;
    }

    /**
    * updateClientPassword
    * @return Array $response
    */
    public function updateClientPassword($old_password, $new_password)
    {
        $response = array();
        $old_pass = mysqli_real_escape_string($this->CONNECTION, $old_password);
        $new_pass = mysqli_real_escape_string($this->CONNECTION, $new_password);

        $query = sprintf("SELECT cid FROM clients WHERE password = '$old_pass'");
        $result = $this->CONNECTION->query($query);

        $data = mysqli_fetch_assoc($result);
        if ($data['cid'] > 0) {
            $client_id = $_SESSION['logged_user']['cid'];
            $update_query = sprintf("UPDATE clients SET password = '.$new_pass.' WHERE cid = '.$client_id.'");
            if($exec = $this->CONNECTION->query($update_query)) {
                $response['status'] = 'success';
            }
        }
        else {
            $response['status'] = 'fail';
        }

        return $response;
    }

    /**
    * updateClientAccountInfo
    * @param Array $userInfoArray
    * @return Array $response
    */
    public function updateClientAccountInfo($userInfoArray) {
        $response = array();
        $client_id = $_SESSION['logged_user']['cid'];
        //protection
        $first_name = mysqli_real_escape_string($this->CONNECTION, $userInfoArray['first_name']);
        $last_name  = mysqli_real_escape_string($this->CONNECTION, $userInfoArray['surname']);
        $kanton     = mysqli_real_escape_string($this->CONNECTION, $userInfoArray['location_name']);
        $address    = mysqli_real_escape_string($this->CONNECTION, $userInfoArray['address']);
        $post       = mysqli_real_escape_string($this->CONNECTION, $userInfoArray['post']);

        $query = sprintf("UPDATE clients SET name='$first_name', surname ='$last_name', address = '$address', post ='$post' WHERE cid=$client_id");
        if ($exec = $this->CONNECTION->query($query)) {
            $response['status'] = 'success';
        }
        else {
            $response['status'] = 'fail';
        }

        return $response;
    }

    /**
    * check_new_bidders
    * Integer $iid
    * @return Array $bidersArray
    */
    public function check_new_bidders($iid)
    {
        $bidersArray = array();
        $item_id = mysqli_real_escape_string($this->CONNECTION, $iid);
        $sql_get_last_biders_info = sprintf("SELECT a.date,l.name, l.country, c.username, b.new_price FROM ".
            "locations l, clients c, bids b, auctions a WHERE a.iid='".$item_id.
            "' AND a.id=b.aid AND c.lid=l.lid AND c.cid=b.cid ORDER BY b.new_price DESC LIMIT 5");
        $result = $this->CONNECTION->query($sql_get_last_biders_info);
        while ($row = mysqli_fetch_array($result)) {
            $tmpArray = array();
            $tmpArray['date'] = $row['date'];
            $tmpArray['location'] = $row['name'];
            $tmpArray['country'] = $row['country'];
            $tmpArray['username'] = $row['username'];
            $tmpArray['bid'] = round($row['new_price'],2);
            $tmpArray['bidder_url'] = "images/location/".$row['country']."/".$row['name'].".png";

            array_push($bidersArray,$tmpArray);
        }

        return $bidersArray;
    }
}
?>
