angular.module('BestBids')
.controller('itemController', ['$scope','$rootScope','$location','$mdDialog', function($scope,$rootScope,$location,$mdDialog){
	$scope.category='';
	$scope.brand='';
	$scope.src='';
	$scope.retail="";
	$scope.min="";
	$scope.save="";
	$scope.current_price = 0;
	$scope.duedate="";
	$scope.iid="";
	$scope.details="";
	$scope.nr_of_bidders=-1;
	$scope.last_bid_date = Date();
	$scope.last_bidders = [];
	$scope.TIME_INTERVAL = 2000;
    //SOCKET ENDPOINT 
    var socket = io.connect('http://localhost:3000');
	//a flag for the button.. when the bid is procceeding you cannot bid ..the button should be disabled
	$scope.you_can_bid = true;
	//when the page loads for the first time
	$scope.$on('$viewContentLoaded', function() {
		//we should get the session data here and upadate the userAccountInformation
		//$rootScope.checkSessionData();
		//NEW APPROACH TESTING --- COMMENT OUT THE OLD ONE
		//var timer = setInterval($scope.check_vital_info, $scope.TIME_INTERVAL);
		//$scope.check_vital_info();
		var paramValue = $location.path();
		var res = paramValue.split("/");
		var selected_item_id = res[2];
		//we have to get the item details
		$scope.item_details(selected_item_id);
        
        socket.emit('bid',selected_item_id);

        socket.on('new message',function(data){

            if (data.item_id == selected_item_id) {
                $scope.last_bidders = [];
                $scope.nr_of_bidders = data.biders.length;
                //reset the array first and then add the new data
                $scope.last_bidders = [];
                $scope.last_bidders = createBiddersArray(data);
                console.log($scope.last_bidders);
                $scope.item_details($scope.iid);

                if ($scope.you_can_bid == false) {
                    $scope.you_can_bid = true;	
                }

                $scope.$apply();
            }
        });
        
	});

	//get item details
	$scope.item_details = function(id) {
		//console.log('time internval is:'+$scope.TIME_INTERVAL);
		var jqxhr = $.post( "php/server.php",{iid:id,command:'item-information'},function() {})
		.done(function(data) {
			var item = jQuery.parseJSON(data);
			$scope.category = item.category;
			$scope.brand = item.brand;
			$scope.src = item.img
			$scope.retail = item.retailprice;
			$scope.min = item.minprice;
			$scope.save = item.retailprice-item.current_price;
			$scope.current_price = item.current_price;
			$scope.duedate = item.duedate;
			//alert('this is the duedate:'+$scope.duedate);
		   /*var current_time = new Date(); 
			var item_deadline = new Date($scope.duedate);
			//check the difference of time to see if there are more than 60 seconds left
			var diff = (item_deadline.getTime() - current_time.getTime())/1000; 
			//check if it has more than 10 minutes then update the TIIME_INTERVAL TO 5 seconds
			*/		
			$scope.iid = item.iid;
			$scope.details = item.description;

			$scope.$apply();
		})
	}

	//get last bidders
	$scope.getBidders = function() {
		var jqxhr = $.post("php/server.php",{command:'show-biders'}, function(){})
		.done(function(data) {
			var response = jQuery.parseJSON(data);
			$scope.nr_of_bidders = response.length;
			$scope.$apply();
		});
	}

	$rootScope.bid = function() {
		$scope.you_can_bid = false;
		if (!$rootScope.isLoggedIn) {
			$rootScope.loginAndBid = true;
			$rootScope.login();
			$scope.you_can_bid = true;
		}
		else {
			if ($rootScope.userAccountInformation.bids<1) {
				//alert('you have no bids would you like to buy');
				$scope.showBuyBidsDialog();
			}
			else {
				//everything is alright you can bid! - WE MIGHT WANT TO MAKE SURE ALSO THAT THE AUCTION IS NOT OVER -
				$scope.bid_on_item();
				//$scope.check_vital_info();
			}
		}
	}

	$scope.check_vital_info = function() {
        //this function is called in a loop the timing depends ..right now we call it every one second
        if ($location.path().includes("item")) {
			var jqxhr = $.post("php/server.php",{item_id:$scope.iid,last_bid:$scope.last_bid_date,command:'live_system'}, function(){})
			.done(function(data) {
				var response = jQuery.parseJSON(data);
				$scope.last_bidders = [];
				$scope.nr_of_bidders = response.length;
				//console.log(response);
				$scope.last_bidders = response;

				$scope.item_details($scope.iid);
				//console.log('ajax polling completed');
				//NEW APPROACH TESTING
				 //setTimeout($scope.check_vital_info(), $scope.TIME_INTERVAL);
				//console.log($scope.last_bidders);
				$scope.$apply();
				if ($scope.you_can_bid == false) {
					$scope.you_can_bid = true;	
				}
			});
		}
	}

	$scope.bid_on_item = function() {
		var jqxhr = $.post("php/server.php",{cid:$scope.userAccountInformation.cid,iid:$scope.iid,command:'bid'}, function(){})
		.done(function(data) {
			var response = jQuery.parseJSON(data);
            //to make double sure we should check also the local time too 
            console.log('this is the data');
            console.log(data);
            console.log('this is the data');
			if (response.status == 'auction_closed') {
				//alert('this auction is closed, the winner will be notified by email in a short period of time');
				$scope.showAuctionClosedMessage();
				//$scope.you_can_bid = true;
			}

            socket.emit('bid',$scope.iid);

            socket.on('new message',function(data){
                
                if (data.item_id == $scope.iid) {
                    $scope.last_bidders = [];
                    $scope.nr_of_bidders = data.biders.length;
                    //reset the array first and then add the new data
                    $scope.last_bidders = [];
                    $scope.last_bidders = createBiddersArray(data);
                    
                    console.log('last bidders');
                    console.log($scope.last_bidders);
                    $scope.item_details($scope.iid);
                    
                    if ($scope.you_can_bid == false) {
                        $scope.you_can_bid = true;	
                    }
                    
                   // $scope.$apply();
                }
            });
            
        
			$scope.item_details($scope.iid);
			$rootScope.checkSessionData();
			$scope.$apply();
		});
	}

	//when there are no bids we show the dialog to ask the user if they want to buy bids
	$scope.showBuyBidsDialog = function() {
        $mdDialog.show({
	      controller: buyBidsDialog,
	      templateUrl: 'views/buy-bids-dialog.html',
	    })

        function buyBidsDialog($scope) {
			$scope.goToBuyBids = function() {
				$location.path('/buy');
				$mdDialog.hide();
			}
        }
    }

	//when the auction is closed we display this message to the user
	$scope.showAuctionClosedMessage = function() {
        $mdDialog.show({
	      controller: auctionClossedDialog,
	      templateUrl: 'views/auction-closed-dialog.html',
	    })

        function auctionClossedDialog($scope) {
			$scope.okPressed = function() {
				$mdDialog.hide();
			}
        }
    }
    
    //process the biders array
    function createBiddersArray(data) 
    {
        var results = [];
        for (var i=0;i<data.biders.length;i++) {
            var tmp = { 
                        date: data.biders[i].date,
                        location: data.biders[i].name,
                        country: data.biders[i].country,
                        username: data.biders[i].username,
                        bid: data.biders[i].new_price.toFixed(2),
                        bidder_url: "images/location/"+data.biders[i].country+"/"+data.biders[i].name+".png" };
            //push the row to the results array
            results.push(tmp);
        }
        
        return results;
    }
    
    
}])
