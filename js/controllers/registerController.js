angular.module('BestBids')
  .controller('registerController', ['$scope','$rootScope',function($scope,$rootScope){
		$scope.progress_circular = false;
		$scope.firstName = '';
		$scope.lastName = '';
		$scope.emailAddress = '';
		$scope.homeAddress = '';
		$scope.postNumber = '';
		$scope.username = '';
		$scope.password = '';
		$scope.promoCode = '';
		$scope.extra_bids = 0;
		$scope.promo_code = '';
		//message 
		$scope.error_message='';
		$scope.showError = false;
		$scope.registration_successful = false;
		//checkers
		$scope.finished_checking_email_validity = false;
		$scope.finished_checking_username_validity = false;
		
		//arrays with countries and kantons
  		$scope.selectedCountry = 'SWIZERLAND';
		$scope.selectedKanton = 'AARGAU';	
		$scope.countries = ['SWIZERLAND','GERMANY','AUSTRIA'];
		$scope.kantons =['AARGAU'];
		
		//when the page loads for the first time
		$scope.$on('$viewContentLoaded', function() {
			$scope.getCantons();
		});
		
		//get all the kantons from the server 
		$scope.getCantons = function() {
			$scope.kantons = [];
			selectedCountry = countryPrefix($scope.selectedCountry);
			var jqxhr = $.post( "php/server.php",{country:selectedCountry,command:'cantons'},function() {})
			.done(function(data) {
				var tmp = jQuery.parseJSON(data);			
				$scope.kantons = prepareKantons(tmp);
				$scope.$apply();
			})
		}
		
		//get kantons for selected country
		$scope.selectCountry = function(country) {
			$scope.selectedKanton = 'AARGAU';
			$scope.selectedCountry = country;		
			$scope.getCantons();
		}
		
		//when button register is clicked
		$scope.btnCreateNewAccountClicked = function(){	
			$scope.error_message = '';
			//next we have to check if we already have the email address in the db or the username
			var jqxhr = $.post("php/server.php",{email:$scope.emailAddress,username:$scope.username,command:'validate_email_and_username'},function(){})
			.done(function(data) {
				var response = jQuery.parseJSON(data);
				if (response['email'] == 'new' && response['username'] == 'new') {
					//we can craete the new account all is fine
					$scope.createNewAccount();
				}
				else if (response['email'] == 'exists') {
					$scope.error_message = $scope.error_message + ' Email already exists!';
					$scope.showError = true;
					$scope.emailAddress = '';
				}
				else if (response['username'] == 'exists') {
					$scope.error_message = $scope.error_message + ' Username already exists!';
					$scope.showError = true;
					$scope.username = '';
				}
				$scope.$apply();
			})
		}
		
		//create new account
		$scope.createNewAccount = function() {
			var jqxhr = $.post("php/server.php",{name:$scope.firstName,surname:$scope.lastName,email:$scope.emailAddress,address:$scope.homeAddress,post:$scope.post,username:$scope.username,password:$scope.password,country:$scope.selectedCountry,kanton:$scope.selectedKanton,code:$scope.promoCode,command:'create_account'},function(){})
			.done(function(data) {
				var response = jQuery.parseJSON(data);
				if (response['status'] == 'success') {
					$scope.extra_bids = response['extra_bids'];
					$scope.promo_code = response['promo_code'];
					$scope.registration_successful = true;
					//log the new user in
					$rootScope.authenticate($scope.username,$scope.password);
				}
				else {
					alert('something went wrong');
				}
				$scope.$apply();
			});
		}
		
  }]);
  
 ////////HELPER FUNCTIONS ////////////////////////////////////////
    var countryPrefix = function (country) {
		if (country=='SWIZERLAND') {
			return 'ch';
		}
		else if (country=='GERMANY') {
			return 'de';
		}
		else if (country=='AUSTRIA') {
			return 'au';
		}
    }
	
	//prepare kantons to show 
	var prepareKantons = function(kantons) {
		var prepared_kantons = [];
		for (var i=0; i<kantons.length; i++) {
			var tmp = kantons[i].replace('_',' ');
			var improved = tmp.toUpperCase();
			prepared_kantons.push(improved);
		}
		
		return prepared_kantons;
	}
	