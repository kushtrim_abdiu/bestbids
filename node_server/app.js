var express = require('express');
var mysql = require('mysql');
app = express(),
server = require('http').createServer(app),
io = require('socket.io').listen(server);

var connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'qTLw4k+H',
    database : 'bestbidcdb'
});

connection.connect();

server.listen(process.env.PORT || 3000);

app.get('/', function(req,res){
    res.sendFile(__dirname + '/../index.html');
});

io.sockets.on('connection',function(socket){
    socket.on('connected',function(data,callback){
         io.sockets.emit('connected message', {msg:'You got connected'});
    });
    //send message event
    socket.on('bid',function(data) {
        //we have to make a query and return the last 5 bidders
        console.log('bid...');
        var iid = data;
        var query = "SELECT l.name, l.country, c.username, b.new_price FROM locations l, clients c, bids b, auctions a "+
                "WHERE a.iid='"+iid+"' AND a.id=b.aid AND c.lid=l.lid AND c.cid=b.cid ORDER BY b.new_price DESC LIMIT 5";

        connection.query(query, function(err,rows,fields){
            if (err) throw error;
            var results = {item_id:iid,biders:rows};
            io.sockets.emit('new message', results);
           // console.log(results);
        });

    });
});

function getLastBidders(data)
{
    var results = [];
    //make sure we dont have any sql injection
   // var iid = mysql.escape(data);
    var iid = data;
    console.log('get bidders...');
    var query = "SELECT l.name, l.country, c.username, b.new_price FROM locations l, clients c, bids b, auctions a "+
            "WHERE a.iid='"+iid+"' AND a.id=b.aid AND c.lid=l.lid AND c.cid=b.cid ORDER BY b.new_price DESC LIMIT 5";

    connection.query(query, function(err,rows,fields){
        if (err) throw error;
        results = rows;
        io.sockets.emit('new message', rows);
       // console.log(results);
    });
    //here we should the the SQL query
  // return 'something is wrong';
    return results;
}
