angular.module('BestBids')
  .controller('closedController', ['$scope','ClosedItems',function($scope,ClosedItems){
  		$scope.closed_items;
		$scope.item_count = 0;
		ClosedItems.getRecentItems().done(function(data){
			$scope.closed_items = jQuery.parseJSON(data);
			console.log($scope.closed_items);
			$scope.item_count = $scope.closed_items.length;
			$scope.$apply();
		});
  }]);