var app = angular.module('BestBids', ['ui.router','ngMaterial'])
.config(function($urlRouterProvider,$stateProvider,$mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('light-blue')
    .accentPalette('pink');

  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('home', {
      url:'/',
      templateUrl:'templates/home.html',
      controller: 'homeController'
    })
    .state('live', {
      url:'/live',
      templateUrl:'templates/live.html',
      controller:'liveController'
    })
    .state('closed', {
      url:'/closed',
      templateUrl:'templates/closed.html',
      controller: 'closedController'
    })
    .state('buy', {
      url:'/buy',
      templateUrl:'templates/buy.html',
      controller: 'buyController'
    })
    .state('how', {
      url:'/how',
      templateUrl:'templates/how.html',
      controller: 'howController'
    })
  .state('register', {
      url:'/register',
      templateUrl:'templates/register.html',
      controller: 'registerController'
    })
    .state('agb', {
      url:'/agb',
      templateUrl:'templates/agb.html',
      controller: 'agbController'
    })
  .state('item', {
      url:'/item/:id',
      templateUrl:'templates/item.html',
      controller: 'itemController'
    })
	.state('account', {
      url:'/account',
      templateUrl:'templates/account.html',
      controller: 'accountController'
    })
});

app.controller('AppCtrl', ['$scope','$rootScope', '$mdSidenav','$mdDialog','$location', function($scope,$rootScope, $mdSidenav,$mdDialog,$location){
	//GLOBAL VARIABLES
	$rootScope.isLoggedIn = false;
	$rootScope.userAccountInformation = [];
    //loginAndBid if the user presses bid but he is not logged in we log him in and then we place the bid
    $rootScope.loginAndBid = false;
    $rootScope.loginAndBuyPackage = false;
    $rootScope.currentSelectedPackage = 0;
    
    $scope.IS_MOBILE = false;
   
	//END OF GLOBAL VARIABLES
	$scope.toggleSidenav = function(menuId) {
		$mdSidenav(menuId).toggle();
    };

	//when the page loads for the first time
	$scope.$on('$viewContentLoaded', function() {
		
		if( navigator.userAgent.match(/Android/i)
		 || navigator.userAgent.match(/webOS/i)
		 || navigator.userAgent.match(/iPhone/i)
		 || navigator.userAgent.match(/iPad/i)
		 || navigator.userAgent.match(/iPod/i)
		 || navigator.userAgent.match(/BlackBerry/i)
		 || navigator.userAgent.match(/Windows Phone/i)
		 ){
		    $scope.IS_MOBILE = true;
		  }
		 else {
		    $scope.IS_MOBILE = false;
		  }
		
		//we should get the session data here and upadate the userAccountInformation
		$rootScope.checkSessionData();

		if ($rootScope.isLoggedIn) {
			$rootScope.registerLoginButtonText = $rootScope.userAccountInformation.first_name;
		}
		console.log($rootScope.userAccountInformation);
	});

	//check if we have session or not
	$rootScope.checkSessionData = function() {
		var jqxhr = $.post("php/server.php",{command:'session-data'}, function(){})
		.done(function(data) {
            console.log(data);
			var results = jQuery.parseJSON(data);
			if (results.status == 'has_session') {
				$rootScope.isLoggedIn = true;
				$rootScope.userAccountInformation = results;
				console.log($rootScope.userAccountInformation);
			}
			else if (results.status =='no_session') {
				$rootScope.isLoggedIn = false;
				$rootScope.userAccountInformation = [];
			}
			$scope.$apply();
		});
	}

	$rootScope.countdownTimer = function(id,lastDate){
		$('#'+id).countdown(lastDate, function(event) {
			$(this).text(event.strftime('%D days %H:%M:%S'));
		});
	};

	//login or register clicked
	$rootScope.login = function($scope) {
		if (!$rootScope.isLoggedIn) {
			$mdDialog.show({
		      controller: loginController,
		      templateUrl: 'views/log-in.html',
		    })
			//the login or register controller
		    function loginController($scope) {		    
				$scope.username="";
				$scope.password="";
				$scope.show_message_wrong_data = false;
				$scope.show_message_empty_fields = false;
				$scope.loginAttempt = false;
				//log me in pressed
				$scope.logMeIn = function() {
					if ($scope.username!='' && $scope.password!='') {
						if ($scope.show_message_empty_fields) {
							$scope.show_message_empty_fields = false;
						}
						//activate the login circular bar
						$scope.loginAttempt = true;
						//authenticate
						$rootScope.authenticate($scope.username,$scope.password);
	
					}
					else {
						$scope.show_message_empty_fields = true;
					}
				}
				//register me pressed
				$scope.registerMe = function() {
					$mdDialog.hide();
					$location.path("/register");
				}
				//authenticate
				$rootScope.authenticate = function(username_input,password_input) {
					var jqxhr = $.post("php/server.php",{username:username_input,password:password_input,command:'authenticate'}, function(){})
					.done(function(data) {
						var results = jQuery.parseJSON(data);
						if (results['status'] == 'success') {
							$rootScope.isLoggedIn = true;
							$rootScope.userAccountInformation = results;
							//the login register button converts to the users name
							
							$mdDialog.hide();
							console.log($rootScope.userAccountInformation);
	                        //check if we have come here from biding - e.i we should place a bid afterwards
	                        if ($rootScope.loginAndBid) {
	                            $rootScope.bid();
	                        }
	                        if ($rootScope.loginAndBuyPackage && $rootScope.currentSelectedPackage>0) {
		                        $rootScope.buyPackage($rootScope.currentSelectedPackage);
		                        $rootScope.loginAndBuyPackage = false;
								$rootScope.currentSelectedPackage = 0;
	                        }
							$scope.$apply();
						}
						else if (results['status'] == 'fail') {
							$scope.show_message_wrong_data = true;
							$scope.username = '';
							$scope.password = '';
						}
						$scope.loginAttempt = false;
						$scope.$apply();
					});
				}
		    }
		}
		else {
			$mdSidenav('right').toggle();
		}
		
	};

    //email us
    $scope.emailUs = function() {
        $mdDialog.show({
	      controller: emailController,
	      templateUrl: 'views/email-us.html',
	    })

        function emailController($scope) {
            $scope.user_email = '';
            $scope.email_send_flag = false;
            $scope.user_message = '';
            //when the button send is clicked
            $scope.sendEmail = function() {
                if ($scope.user_email != '') {
                    //then we can procceed and send the email to us
                    var jqxhr = $.post("php/server.php",{command:'sendUsEmail',address:$scope.user_email,message:$scope.user_message}, function(){})
            		.done(function(data) {
            			$scope.send_email_status = 'Email was sent successfully!'
                        $mdDialog.hide();
            		});
                }
                else {
                    $scope.email_send_flag = true;
                }
            }
        }
    }

	//click on more account details
	$scope.showUserAccountDetails = function() {
		$mdSidenav('right').toggle();
		$location.path("/account");
	}

	//logout clicked
	$scope.logOut = function() {
		var jqxhr = $.post("php/server.php",{command:'user-logout'}, function(){})
		.done(function() {
			$rootScope.checkSessionData();
			$scope.$apply();
		});
	}
	
	$scope.goHome = function() {
		$location.path("/");
	}

}]);
