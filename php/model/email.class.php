<?php
    /**
    * THis is the EMAIL CLASS
    */
    class Email
    {
        /**
        * onAccountCreation - sends email when the account is created
        * @param String $name
        * @param String $email
        * @param String $username
        * @param String $password
        * @return void
        */
        public function onAccountCreation($name, $email, $username, $password)
        {
            $to = $email;
            $subject = "Willkommen bei BestBids.ch";

            $message = "
            <html><head><title>Hallo, ".$name."</title></head>
                <body><p>Danke, dass Du ein Teil von uns bist. Unten findest Du deine Login Informationen.</p><p>Viel Spass beim bieten!</p>
                    <table><tr><th>username</th><th>password</th></tr><tr><td>".$username."</td><td>".$password."</td></tr></table>
                </body>
            </html>
            ";

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $headers .= 'From: <noreply@bestbids.ch>' . "\r\n";
            $headers .= 'Cc: bestbich@gmail.com' . "\r\n";

            mail($to,$subject,$message,$headers);
        }

        /**
        * contactUs - used when users send us an email from the contact us button
        * @param String $address - their email address
        * @param String $message
        * @return void
        */
        public function contactUs($address, $message)
        {
            $to = "bestbidch@gmail.com";
            $subject = "Email von Kunden";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $headers .= 'From: <'.$address.'>' . "\r\n";
            mail($to, $subject, $message, $headers);
        }

        /**
        * notifyWinner - sends an email to the winner of the auction
        * @param Array $winner_info
        * @return void
        */
        public function notifyWinner($winner_info)
        {
            $to = $winner_info['email'];
            $subject = "Gratulation";

            $message = "
            <html><head><title>Gratulation, ".$winner_info['client_name']."</title></head>
                <body><p>Wir freuen uns Dir mitzuteilen, dass Du die soeben beendete Auktion gewonnen hast., ".$winner_info['item_name']." Bitte überprüfe deine Wohnadresse. In den nächsten Tagen senden wir Dir dein Produkt.</p>
                <p>Viel Spass beim bieten!</p>
                <table>
                    <tr>
                        <th>Adresse</th>
                        <th>Post</th>
                    </tr>
                    <tr>
                        <td>".$winner_info['address']."</td>
                        <td>".$winner_info['post']."</td>
                    </tr>
                </table>
                </body>
            </html>";

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $headers .= 'From: <noreply@bestbids.ch>' . "\r\n";
            $headers .= 'Cc: bestbich@gmail.com' . "\r\n";

            mail($to, $subject, $message, $headers);
        }

        /**
        * notifyClientForBidsReturned - if the auction is finished and the item is sold
        * we notify our clients about it and we return their bids + some extra
        * @param String $name
        * @param String $email
        * @param Integer $frequency
        * @param Double $twenty_percent
        * @param String $item_name
        * @return void
        */
        public function notifyClientForBidsReturned($name, $email, $frequency, $twenty_percent, $item_name)
        {
            $to = $name;
            $subject = "Beendete Auktion ohne Gewinner!";

            $message = "
            <html><head><title>Hallo, ".$name."</title></head>
                <body>
                    <p>Die Auktion für ".$item_name.". ist beendet. Leider hat niemand gewonnen! Wir haben Dir deine Bids für diese Auktion wieder gutgeschrieben. Zusätzlich erhältst Du 10% mehr Bids. ".$frequency." bids back + ".$twenty_percent." mehr (10% von deinen Bids)</p>
                    <p>Viel Glück bei nächsten Mal!</p>
                </body>
            </html>";

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $headers .= 'From: <noreply@bestbids.ch>' . "\r\n";
            $headers .= 'Cc: bestbich@gmail.com' . "\r\n";

            mail($to, $subject, $message, $headers);
        }

        /**
        * customEmail - this is a custom email
        * @param String $to - is the email address we want tos end the email to
        * @param String $message
        * @return void
        */
        public function customEmail($to,$message)
        {
            $subject = "Custom message!";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $headers .= 'From: <noreply@bestbids.ch>' . "\r\n";
            $headers .= 'Cc: bestbich@gmail.com' . "\r\n";

            mail($to,$subject,$message,$headers);
        }
    }

?>
