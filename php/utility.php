<?php
	/**
	* THESE ARE SOME UTILITY FUNCTIONS
	*/
	require_once('model/client.class.php');

	/**
	* create_promotional_code - generates a promotional code
	* @return String $res  - the promotional code
	*/
	function create_promotional_code() {
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$res = "";
		for ($i = 0; $i < 10; $i++) {
			$res .= $chars[mt_rand(0, strlen($chars)-1)];
		}

		return $res;
	}

	/**
	* fix_canton - makes the canton name user friendly ...
	* @param String $kanton
	* @return String $canton
	*/
	function fix_canton($kanton) {
		$lowercase = strtolower($kanton);
		$canton = str_replace(' ', '_', $kanton);

		return $canton;
	}

	/**
	* password_hash
	* @param String $password
	* @param String $promo
	* @return String hashed password
	*/
	function password_hash($password, $promo) {
		return md5($promo . $password);
	}

	/**
	* validate_promo
	* @param String $code
	* @return Array - if the promo code is correct +2 extra bids | +0
	*/
	function validate_promo($code) {
		$response = array();
		$client = new Client();
		$is_valid = $client->validate_promo_code($code);

		if ($is_valid) {
			$response['extra_bids'] = 2;
			$response['message'] = 'match';
		}
		else {
			$response['extra_bids'] = 0;
			$response['message'] = 'no_match';
		}
		return $response;
	}

	/**
	* validate_promo
	* @param String $username
	* @param String $password - we dont need it right now
	* @return String - password generated
	*/
	function generate_password_from_input($username, $password) {
		$client = new Client();
		$promo_code = $client->get_client_promo_code($username);
		return md5($promo_code.$password);
	}
?>
