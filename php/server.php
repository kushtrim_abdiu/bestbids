<?php
    /**
    * THIS IS LIKE AN API GATEWAY
    * IT NEEDS TO BE MORE CLEANER AND IT SHOULD BE REWRITTEN
    * TO HANDLE THE ROUTING BETTER .. AND IT SHOULD BE BROKEN DOWN INTO SMALLER API GATEWAYs
    */
    session_start();
	require_once('model/client.class.php');
	require_once('model/admin.class.php');
	require_once('model/utils.php');
	require_once('model/email.class.php');
	require_once('json/item_json_utility.php');
	require_once('utility.php');
    require_once('stripe/init.php');

    if (!empty($_FILES)) {
        //upload the image of the item that is being created
		$ds = DIRECTORY_SEPARATOR;  //1
		$storeFolder = '../images/items';   //2
		//$storeFolder = 'images/items';
		//$extension =pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$extension = pathinfo($_FILES['item_image']['name'], PATHINFO_EXTENSION);
		$newName = time();
		$random = rand(100,999);
		$name = $newName . $random.'.'. $extension;
		$tempFile = $_FILES['file']['tmp_name'];          //3
		$targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //4
		//$targetFile =  $targetPath.$name;  //5
		$targetFile = '../images/items/' . $name;


		//move_uploaded_file($tempFile,$targetFile); //6
		move_uploaded_file($_FILES["item_image"]["tmp_name"],"../images/items/".$name);

        //the input from the admin the item information
		$p_name = $_POST['p_name'];
		$brand = $_POST['brand'];
		$min_price = $_POST['min'];
		$retail_price = $_POST['retail'];
		$deadline = $_POST['deadline'];
		$category = $_POST['category'];
		$description = $_POST['description'];

		//save this item into the database
		$admin= new Admin();
		$item_id = $admin->uploadNewItem($p_name, $brand, $category, $min_price,
            $retail_price, $deadline, $description, 'active', $name);

		$admin->createNewAuction($item_id);
		$email = new Email(); //just a test email
		$message = 'The photo you uploaded was named:'. $name;
		$email->customEmail('kushtrim.abdiu.16@gmail.com', $message);
    }

    if (isset($_POST['command'])) {
        //if the request is to authenticate the client
    	if ($_POST['command'] == 'authenticate' && (isset($_POST['username']) && isset($_POST['password']))) {
			$user= new Client();
            //we hash the input the user has sent as his password then we compare it
			$password = generate_password_from_input($_POST['username'], $_POST['password']);
			$userDataArray = $user->authenticateClient($_POST['username'], $password);
			if ($userDataArray['status'] == 'success') {
                //we create a user session
				$_SESSION['logged_user'] = $userDataArray;
			}

			$encoded_data = json_encode($userDataArray);
			print_r($encoded_data);
    	}
        else if ($_POST['command'] == 'create_account') {
            //CREATE A NEW USER ACCOUNT
            $client = new Client();
            $response = array();
			//utility_function
			$kanton = fix_canton($_POST['kanton']);
            $cid = $client->getCantonID($kanton);
			$promo_code = create_promotional_code();
			$password = password_hash($_POST['password'], $promo_code);
			$extra_bids = validate_promo($_POST['code']);

			$bids = 5 + $extra_bids['extra_bids'];

			$response['status'] = $client->insertClient($_POST['name'],$_POST['surname'],$_POST['email'],
                $_POST['username'],$password,$_POST['address'], $_POST['post'],$bids,$cid,$promo_code);
            //we return the created promotianal code so the client can share it
            //with his friends .. if they register he gets extra bids
            $response['promo_code'] = $promo_code;
			$response['extra_bids'] = $extra_bids['extra_bids'];

			$json_response = json_encode($response);

			//email the account info to the user
			$email = new Email();
			$email->onAccountCreation($_POST['name'], $_POST['email'], $_POST['username'], $_POST['password']);

			print_r($json_response);
	   }
        else if ($_POST['command'] == 'session-data'){
            //CHECK FOR USER SESSION
			if (isset($_SESSION['logged_user']['cid']) ) {
				$_SESSION['logged_user']['status'] = 'has_session';
			}
			else {
				$_SESSION['logged_user']['status'] = 'no_session';
			}
            //if we have a session we return the logged user information
			$jsonData = json_encode($_SESSION['logged_user']);
			print_r($jsonData);
        }
        else if ($_POST['command'] == 'admin-authenticate') {
            //authenticate the admin .. for now its FOR TESTING JUST
			$confirmArray = array();
			if($_POST['username'] == 'egzonanderim' && $_POST['password'] == '2015egzonanderim2015') {
				$_SESSION['admin-name'] = 'admin';
				$confirmArray['confirm'] = 'YES';
			}
			else{
				$confirmArray['confirm'] = 'NO';
			}

			$jsonConfirm =json_encode($confirmArray);
			print_r($jsonConfirm);
        }
        //this function is called when the user lands in the admin-logged-in page to verify that the user has session
        else if ($_POST['command'] == 'has-admin-session') {
            //check if there is an admin session
            $arrayResponse = array();
            if (isset($_SESSION['admin-name'])) {
                $arrayResponse['confirm'] = 'yes';
                $arrayResponse['name'] = $_SESSION['admin-name'];
            }
            else{
                $arrayResponse['confirm'] = 'no';
            }
            $jsonResponse = json_encode($arrayResponse);
            print_r($jsonResponse);
        }
        else if ($_POST['command'] == 'admin-logout') {
            //logout the admin
            $responseArray =array();
            unset($_SESSION['admin-name']);
            if (isset($_SESSION['admin-name'])) {
                $responseArray['status'] = 'fail';
            }
            else {
                $responseArray['status'] = 'success';
            }
            $jsonResponse =json_encode($responseArray);
            print_r($jsonResponse);
        }
        else if ($_POST['command'] == 'auctions') {
            //GETS ALL THE ACTIVE AUNCTION ITEMS
            $client = new Client();
            $results = $client->getAllActiveItems();
            $results_encoded =json_encode($results);
            print_r($results_encoded);
        }
		else if ($_POST['command'] == 'latest-auctions') {
            //gets the last active items i.e. the latest auctions
            $client = new Client();
            $results = $client->getLatestActiveItems();
            $results_encoded =json_encode($results);
            print_r($results_encoded);
		}
		else if ($_POST['command'] == 'get-current-item-id') {
			echo $_SESSION['item-id'];
		}
        else if ($_POST['command'] == 'auctions-category') {
            //THIS FUNCTION IS CALLED ON AUCTIONS PAGE TO LOAD THE ITEMS THAT BELONG TO THE SPECIFIED CATEGORY
            $category = $_POST['category'];
            $client = new Client();
            $results = $client->getActiveItemsOfCategory($category);
            $results_encoded = json_encode($results);
            print_r($results_encoded);
        }
        else if ($_POST['command'] == 'closed-auctions') {
            $client = new Client();
            $results = $client->getAllClosedAuctions();
            $results_encoded = json_encode($results);
            print_r($results_encoded);
        }
		else if ($_POST['command'] == 'item_id') {
            //create an item session so we can access it from item.html page
			$_SESSION['item-id'] = $_POST['itemid'];
			echo $_POST['itemid'];
		}
        else if ($_POST['command'] == 'item-information') {
			$id = $_POST['iid'];
            $ID = $_SESSION['item-id'];
            $client = new Client();
            $results = $client->getItem($id);
            $results_encoded = json_encode($results);
            print_r($results_encoded);
        }
        else if($_POST['command']=='user-info'){
            if(isset($_SESSION['username'])){
                $arrayData=array();
                $client = new Client();
                $arrayData['bids'] = $client->getClientBids($_SESSION['username']);
                $arrayData['username'] = $_SESSION['username'];

                print_r(json_encode($arrayData));
            }
        }
        else if ($_POST['command'] == 'pre-bid') {
            //before the user is allowed to bid we should check
            //if he has bids and if the item is still active .. etc...
            $iid = $_SESSION['item-id'];
            $cid = $_SESSION['id'];
            $client = new Client();
            $hasBids = 1;
            $hasTime = 1;
            $item_deadline = $client->checkItemDeadline($iid);
            $current_time = date('Y-m-d H:i:s');
            $diff = strtotime($item_deadline) - strtotime($current_time);
            //get client bids too
            $client_bids = $client->getUserBids($cid);
            if ($client_bids < 1) {
                $hasBids=0;
            }
            if ($diff < 0) {
                $hasTime=0;
            }

            $arrayInfo = array();
            $arrayInfo['hasBids'] = $hasBids;
            $arrayInfo['hasTime'] = $hasTime;
            $arrayInfo['difference'] = $diff;
			$arrayInfo['isloggedin'] = $cid;
            $encode_data = json_encode($arrayInfo);
            print_r($encode_data);
        }
        else if ($_POST['command'] == 'bid') {
	        $item_id = intval($_POST['iid']);
            $client = new Client();
            //this function is used to place a bid
			$response = $client->placeBid($item_id,$_POST['cid']);
			$items_json = new Item_JSON($item_id);
			$items_json->update_timestamp_for_item($item_id);

			$encode_data = json_encode($response);

			print_r($encode_data);
        }
        else if ($_POST['command'] == 'cantons') {
            $country = $_POST['country'];
            $client = new Client();
            $results = $client->getCantons($country);
			$jsonResults = json_encode($results);
            print_r($jsonResults);
        }
        else if ($_POST['command'] == 'check_username') {
            //checks if the username is already taken
            $username = $_POST['username'];
            $client = new CLient();
            $answer = $client->checkIfUsernameExists($username);
            $encoded_answer = json_encode($answer);
            print_r($encoded_answer);
        }
        else if ($_POST['command']=='get-all-brands') {
            $client = new Client();
            $arrayBrands = $client->getAllBrands();
            $encoded_brands = json_encode($arrayBrands);
            print_r($encoded_brands);
        }
        else if ($_POST['command'] == 'items-of-brand') {
            $brand = $_POST['brand'];
            $client = new Client();
            $arrayItems = $client->getItemsOfBrand($brand);
            $encoded_items = json_encode($arrayItems);

            print_r($encoded_items);
        }
        else if ($_POST['command'] == 'validate_email') {
            $client = new Client();
            $counter = $client->validateEmail($_POST['email']);
            echo $counter;
        }
        else if ($_POST['command'] == 'latest_items') {
            $client = new Client();
            $itemsArray =$client->getLatestItems();
            $encoded_items = json_encode($itemsArray);
            print_r($encoded_items);
        }
		else if ($_POST['command'] == 'sendUsEmail') {
			$address = $_POST['address'];
			$message = $_POST['message'];

			$email = new Email();
			$email->contactUs($address,$message);
		}
        else if ($_POST['command'] == 'show-biders') {
            //the last 5 biders of the item selected
            $client = new Client();
            $arrayBiders = $client->getLastBidersOfItem($_SESSION['item-id']);
            $encodedBiders = json_encode($arrayBiders);
            print_r($encodedBiders);
        }
        else if ($_POST['command'] == 'user-logout') {
			session_unset();
        }
        else if ($_POST['command'] == 'check-user-session') {
            $responseArray =array();
            if (isset($_SESSION['id-name']) && isset($_SESSION['username']) && isset($_SESSION['bids'])) {
                $responseArray['status'] = 'success';
            }
            else {
                $responseArray['status'] = 'fail';
            }
            $jsonResponse = json_encode($responseArray);
            print_r($jsonResponse);
        }
        else if ($_POST['command']=='stripe-service') {
            //we should have A PAYMENT CLASS AND HANDLE THIS THERE
			$response = array();
			$admin = new Admin();
            \Stripe\Stripe::setApiKey("sk_test_sN8I2434w7Ir7AIsbZpB0Hm2");
            // Get the credit card details submitted by the form
            $token = $_POST['stripe'];
			$package_id = $_POST['pid'];
			//get package details
			$package = $admin->getPackageInfo($package_id);
            // Create the charge on Stripe's servers - this will charge the user's card
            try {
                $charge = \Stripe\Charge::create(array(
                  "amount" => $package['price']*100, // amount in cents, again
                  "currency" => "sek",
                  "source" => $token,
                  "description" => "this is just a description")
                );
                //the charging went successfully now we have to register the package purchase to the database
				$client = new Client();
				$response = $client->buyPackage($package);

            } catch(\Stripe\Error\Card $e) {
                // The card has been declined
				echo $e;
            }

			$json_response = json_encode($response);
			print_r($json_response);
        }
        else if ($_POST['command'] == 'check-item-status') {
            //first we have to get all the items that have their status as active
            $client = new Client();
            $activeItems = $client->getAllActiveItems();
            $client->refreshItemStatuses($activeItems);
        }
		else if ($_POST['command'] == 'get-total-item-nr') {
            //first we have to get all the items that have their status as active
            $admin = new Admin();
            $totalNr = $admin->getNumberOfItems();
            echo $totalNr;
        }
		else if ($_POST['command'] == 'get-total-item-nr-active') {
            //first we have to get all the items that have their status as active
            $admin = new Admin();
            $totalNr = $admin->getNumberOfActiveItems();
            echo $totalNr;
        }
		else if ($_POST['command'] == 'get-total-item-nr-sold') {
            //first we have to get all the items that have their status as active
            $admin = new Admin();
            $totalNr = $admin->getNumberOfSoldItems();
            echo $totalNr;
        }
		else if ($_POST['command'] == 'get-total-item-nr-passed') {
            //first we have to get all the items that have their status as active
            $admin = new Admin();
            $totalNr = $admin->getNumberOfPassedItems();
            echo $totalNr;
        }
		else if ($_POST['command'] == 'get-total-client-nr') {
            //first we have to get all the items that have their status as active
            $admin = new Admin();
            $totalNr = $admin->getNumberOfClients();
            echo $totalNr;
        }
		else if ($_POST['command'] == 'get_user_bids') {
			$cid = $_SESSION['id'];
			$client = new Client();
			echo $client->getUserBids($cid);
		}

		else if ($_POST['command'] == 'test') {
			$array_test = array();
			$array_test['name'] = 'Kushtrim';
			$array_test['surname'] = 'Abdiu';

			$json_data = json_encode($array_test);
			print_r($json_data);

			return $json_data;
		}
		else if ($_POST['command'] == 'get-packages') {
			$admin = new Admin();
			$packages = $admin->getBidPackages();
			$json = json_encode($packages);
			print_r($json);
		}

////////NEW RUTINES ///////////////////////////////////////////////////////////
		else if ($_POST['command'] == 'validate_email_and_username') {
			//first validate the username
			$response = array();
			$client = new Client();
			$email = $client->validateEmail($_POST['email']);
            $username = $client->checkIfUsernameExists($_POST['username']);

			$response['email'] = $email;
			$response['username'] = $username;
			$json_data = json_encode($response);
			print_r($json_data);
		}
		else if ($_POST['command'] == 'update_email') {
			$client = new Client();
			$response = $client->updateClientEmail($_POST['new_email']);
			$json_response = json_encode($response);
			print_r($json_response);
		}
		else if ($_POST['command'] == 'update_password') {
			$client = new Client();
			$old_password = generate_password_from_input($_SESSION['logged_user']['username'],$_POST['old_pass']);
			$new_password = generate_password_from_input($_SESSION['logged_user']['username'],$_POST['new_pass']);

			$response = $client->updateClientPassword($old_password,$new_password);
			$json_response = json_encode($response);

			print_r($json_response);
		}
		else if ($_POST['command'] == 'update_user_account_info') {
			$user_info = $_POST['user'];
			$client = new Client();
			$response = $client->updateClientAccountInfo($user_info);
			$json_response = json_encode($response);

			print_r($json_response);
		}
		else if ($_POST['command'] == 'get_item_bidders') {
			$client = new Client();
			$response = $client->check_new_bidders($_POST['item_id']);
			$json_response = json_encode($response);

			print_r($json_response);
		}
		else if ($_POST['command'] == 'live_system') {
			$client = new Client();
			$response = $client->check_new_bidders($_POST['item_id']);
			$json_response = json_encode($response);
			print_r($json_response);

			/*
			$client = new Client();
			$response = $client->check_new_bidders($_POST['item_id']);
			$json_response = json_encode($response);

			$item_id = intval($_REQUEST['item_id']);
			$old_timestamp = $_REQUEST['timestamp'];
			$items_json = new Item_JSON($item_id);
			$new_timestamp = $items_json->get_timestamp_for_item($item_id);

			while ($new_timestamp<=$old_timestamp) {
				sleep(1);
				clearstatcache();
				$new_timestamp = $items_json->get_timestamp_for_item($item_id);
			}

			$complete_response['server_timestamp'] = $new_timestamp;
			$complete_response['bidders'] = $response;
			$complete_response_json = json_encode($complete_response);

			print_r($complete_response_json);
			flush();
			//print_r($json_response);

			$item_id = intval($_REQUEST['item_id']);
			$old_timestamp = $_REQUEST['timestamp'];
			$items_json = new Item_JSON($item_id);
			$new_timestamp = $items_json->get_timestamp_for_item($item_id);
			while ($new_timestamp<=$old_timestamp) {
				usleep(10000);
				clearstatcache();
				$new_timestamp = $items_json->get_timestamp_for_item($item_id);
			}

			*/
			//response if it breaks out of the loop we have new
/*			$client = new Client();
			$response = array();
			$response['server_timestamp'] = $items_json->get_timestamp_for_item($item_id);
			$response = $client->check_new_bidders($item_id);
			$json_response = json_encode($response);
			print_r($json_response);
			*/
	    /*
			$item_id = intval($_POST['item_id']);

			$items_json = new Item_JSON($item_id);
			$new_timestamp = $items_json->get_timestamp_for_item($item_id);
			$old_timestamp = $_REQUEST['timestamp'];
			$client = new Client();
			$response = array();

			$response['status'] = 'no_change';
			if ($new_timestamp>$old_timestamp) {
				$response = $client->check_new_bidders($item_id);
				$response['server_timestamp'] = $new_timestamp;
				$response['status'] = 'has_change';

			}
			else {
				sleep(1);

			}
			//sleep(1);
			//check if we have new bidders
			$json_response = json_encode($response);
			print_r($json_response);
			|*/
		}



    }
?>
